import React, { Component } from 'react';
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  Button,
  FormControl,
} from 'react-bootstrap';
import '../scss/Navbar.scss';

export default class NavBar extends Component {
  render() {
    return (
      <div>
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand href='/' className='brand'>
            <h2>Mutual Fund</h2>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ml-auto d-flex flex-column'>
              <Nav className='py-0'>
                <Navbar.Text className='my-name py-0'>
                  By Raja Basumatary
                </Navbar.Text>
              </Nav>
              <Nav className='py-0'>
                <Navbar.Text className='public-repo py-0'>
                  Click here -{' '}
                  <a
                    href='https://gitlab.com/rajabasu/mutual-fund'
                    target='_blank'
                  >
                    Public git repo
                  </a>
                </Navbar.Text>
              </Nav>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
