import React from 'react';
import '../scss/FundDetails.scss';

const FundDetails = ({
  name,
  schemeCategory,
  schemeName,
  schemeType,
  isLoading,
}) => {
  return isLoading ? (
    <div></div>
  ) : (
    <div className='container d-flex justify-content-center'>
      <div className='fund-details'>
        <div className='fund-name'>{name}</div>
        <div className='scheme-category'>
          Scheme Category : <strong>{schemeCategory}</strong>
        </div>
        <div className='scheme-name'>
          Scheme Name : <strong>{schemeName}</strong>
        </div>
        <div className='scheme-type'>
          Scheme Type : <strong>{schemeType}</strong>
        </div>
      </div>
    </div>
  );
};

export default FundDetails;
