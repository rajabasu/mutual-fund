import React, { useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import DisplayChart from './DisplayChart';
import FundDetails from './FundDetails';
import '../scss/MutualFund.scss';

const MutualFund = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [fundHouse, setFundHouse] = useState();
  const [schemeCategory, setSchemeCategory] = useState();
  const [schemeName, setSchemeName] = useState();
  const [schemeType, setSchemeType] = useState();
  const [x, setX] = useState();
  const [y, setY] = useState();

  let labels = [],
    values = [];

  let tempLabels, tempValues;

  const fetchData = async (data) => {
    const response = await axios.get(`https://api.mfapi.in/mf/${data}`);
    const resData = response.data;

    setFundHouse(resData.meta.fund_house);
    setSchemeCategory(resData.meta.scheme_category);
    setSchemeName(resData.meta.scheme_name);
    setSchemeType(resData.meta.scheme_type);

    for (let i = 0; i < resData.data.length; i++) {
      tempLabels = {
        label: moment(resData.data[i].date, 'Do MM YYYY').format('MMM Do YYYY'),
      };
      labels.unshift(tempLabels);
    }

    for (let i = 0; i < resData.data.length; i++) {
      tempValues = {
        value: resData.data[i].nav,
      };
      values.unshift(tempValues);
    }

    setX(labels);
    setY(values);
    setIsLoading(false);
  };

  return (
    <div className='mutual-fund'>
      <h1 className='text-center'>Mutual Fund Data</h1>
      <div>
        <DisplayChart
          name={fundHouse}
          schemeCategory={schemeCategory}
          schemeName={schemeName}
          schemeType={schemeType}
          labels={x}
          data={y}
          isLoading={isLoading}
        />
      </div>
      <div className='btn-wrapper'>
        <button
          className='btn'
          id='1'
          onClick={() => {
            fetchData('100055');
          }}
        >
          Aditya Birla Sun Life Mutual Fund
        </button>
        <button
          className='btn'
          onClick={() => {
            fetchData('101000');
          }}
        >
          Tata Mutual Fund
        </button>
        <button
          className='btn'
          onClick={() => {
            fetchData('100842');
          }}
        >
          Nippon India Mutual Fund
        </button>
        <button
          className='btn'
          onClick={() => {
            fetchData('102201');
          }}
        >
          SBI Mutual Fund
        </button>
      </div>
      <div>
        <FundDetails
          name={fundHouse}
          schemeCategory={schemeCategory}
          schemeName={schemeName}
          schemeType={schemeType}
          isLoading={isLoading}
        />
      </div>
    </div>
  );
};

export default MutualFund;
