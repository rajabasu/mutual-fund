import React from 'react';

import ReactFC from 'react-fusioncharts';
import FusionCharts from 'fusioncharts';
import ScrollLine2D from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

import IfEmpty from './IfEmpty';
import '../scss/DisplayChart.scss';

ReactFC.fcRoot(FusionCharts, ScrollLine2D, FusionTheme);

const DisplayChart = ({ name, labels, data, isLoading }) => {
  const chartConfigs = {
    type: 'scrollline2d',
    width: '100%',
    height: '400',
    dataFormat: 'json',
    dataSource: {
      chart: {
        theme: 'fusion',
        caption: name,
        captionFontSize: '25',
        captionFontBold: '1',
        xAxisName: 'Date',
        yAxisName: 'Nav',
        lineThickness: '3',
        flatScrollBars: '1',
        scrollheight: '10',
        numVisiblePlot: '12',
        showHoverEffect: '1',
      },
      categories: [
        {
          category: labels,
        },
      ],
      dataset: [
        {
          data: data,
        },
      ],
    },
  };

  return isLoading ? (
    <IfEmpty />
  ) : (
    <div className='container-fluid display-chart'>
      <ReactFC {...chartConfigs} />
    </div>
  );
};

export default DisplayChart;
