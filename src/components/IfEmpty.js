import React from 'react';
import '../scss/IfEmpty.scss';

const IfEmpty = () => {
  return (
    <div className='container ifempty'>
      <div className='text-center'>Choose your mutual fund below</div>
    </div>
  );
};

export default IfEmpty;
