import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

import Navbar from './components/Navbar';
import MutualFund from './components/MutualFund';

function App() {
  return (
    <div className='App'>
      <Navbar />
      <MutualFund />
    </div>
  );
}

export default App;
