## Frontend Test - EduFund

### Submitted by Raja Basumatary

### Publicly hosted site for this assignment - [Link](https://master.dwg2ffqen03vc.amplifyapp.com/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## The libraries that I have installed is -

1. ## [React BootStrap](https://react-bootstrap.github.io/)

   ### `npm install react-bootstrap bootstrap`

   For ease of use in styling.

2. ## [Node-SASS](https://www.npmjs.com/package/node-sass)

   ### `npm install node-sass`

   For styling.

   Add the following script in the package.json file so that when we run this script it will watch every .scss file in the scss/ folder, then save the compiled css in css/ folder every time we change a .scss file.

   ```
   "scripts": {
       "scss": "node-sass --watch scss -o css"
   }
   ```

3. ## [Moment Js](https://momentjs.com/)

   ### `npm install moment --save`

   Used for time modification.

4. ## [FusionCharts](https://www.fusioncharts.com/dev/getting-started/react/your-first-chart-using-react)

   ### `npm install fusioncharts react-fusioncharts --save`

   Used for displaying the charts for the dynamically fetched data.

5. ## [Axios](https://www.npmjs.com/package/axios)

   ### `npm install axios`

   Used for api calls.
